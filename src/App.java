import com.devcamp.models.Customer;
import com.devcamp.models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Le Huu Nghi", 10);
        System.out.println("Customer 1");
        System.out.println(customer1);

        Customer customer2 = new Customer(2, "Nguyen Hoang Nam", 20);
        System.out.println("Customer 2");
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(1, customer1, 1000000);
        System.out.println("Invoice 1");
        System.out.println(invoice1);

        Invoice invoice2 = new Invoice(2, customer2, 2000000);
        System.out.println("Invoice 2");
        System.out.println(invoice2);
    }
}
