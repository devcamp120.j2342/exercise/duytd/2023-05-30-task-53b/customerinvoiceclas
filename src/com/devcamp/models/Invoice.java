package com.devcamp.models;

public class Invoice {
    private int id;
    private Customer customer;
    double amount;

    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerID() {
        return customer.getId();
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public int getDiscount() {
        return customer.getDiscount();
    }

    public double getAmountAfterDiscount(double amount, int discount) {
        double discountAmount = amount * (discount / 100);
        double amountAfterDiscount = amount - discountAmount;
        return amountAfterDiscount;
    }

    @Override
    public String toString() {
        return "Invoice [id="+this.id+", Customer[id=" + customer.getId() + ", name=" + customer.getName() + ", discount="+customer.getDiscount()+"], amount=" + this.amount + "]";
    }

    
}
